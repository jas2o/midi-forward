﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Midi;

namespace MIDI_Forward
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        bool forwarding = false;
        InputDevice MIDI_IN = null;
        OutputDevice MIDI_OUT_1= null;
        OutputDevice MIDI_OUT_2 = null;

        private void Form1_Load(object sender, EventArgs e)
        {
            foreach (InputDevice device in InputDevice.InstalledDevices)
            {
                ddlMidiIn.Items.Add(device.Name);
            }

            foreach (OutputDevice device in OutputDevice.InstalledDevices)
            {
                ddlMidiOut1.Items.Add(device.Name);
                ddlMidiOut2.Items.Add(device.Name);
            }
        }

        private void btnForward_Click(object sender, EventArgs e)
        {
            ddlMidiIn.Enabled = forwarding;
            ddlMidiOut1.Enabled = forwarding;
            ddlMidiOut2.Enabled = forwarding;

            forwarding = !forwarding;
            //btnForward.Enabled = forwarding;

            if(forwarding) { //Turn it on
                btnForward.Text = "Forwarding";

                MIDI_IN.Open();
                MIDI_OUT_1.Open();
                MIDI_OUT_2.Open();

                MIDI_IN.NoteOn += new InputDevice.NoteOnHandler(NoteOn);
                MIDI_IN.NoteOff += new InputDevice.NoteOffHandler(NoteOff);
                MIDI_IN.PitchBend += new InputDevice.PitchBendHandler(PitchBend);
                MIDI_IN.ProgramChange += new InputDevice.ProgramChangeHandler(ProgramChange);
                MIDI_IN.ControlChange += new InputDevice.ControlChangeHandler(ControlChange);
                MIDI_IN.SysEx += new InputDevice.SysExHandler(SysEx);

                int bpm = 180;
                MIDI_IN.StartReceiving(new Clock(bpm), true);

            } else { //Turn it off
                btnForward.Text = "Sleeping";

                MIDI_IN.StopReceiving();
                MIDI_IN.RemoveAllEventHandlers();

                MIDI_IN.Close();
                MIDI_OUT_1.Close();
                MIDI_OUT_2.Close();
            }
        }

        private void ddlMidiIn_SelectedIndexChanged(object sender, EventArgs e)
        {
            MIDI_IN = InputDevice.InstalledDevices[ddlMidiIn.SelectedIndex];
        }

        private void ddlMidiOut1_SelectedIndexChanged(object sender, EventArgs e)
        {
            MIDI_OUT_1 = OutputDevice.InstalledDevices[ddlMidiOut1.SelectedIndex];
        }

        private void ddlMidiOut2_SelectedIndexChanged(object sender, EventArgs e)
        {
            MIDI_OUT_2 = OutputDevice.InstalledDevices[ddlMidiOut2.SelectedIndex];
        }

        public void NoteOn(NoteOnMessage msg) {
            MIDI_OUT_1.SendNoteOn(msg.Channel, msg.Pitch, msg.Velocity);
            MIDI_OUT_2.SendNoteOn(msg.Channel, msg.Pitch, msg.Velocity);
        }

        public void NoteOff(NoteOffMessage msg) {
            MIDI_OUT_1.SendNoteOff(msg.Channel, msg.Pitch, msg.Velocity);
            MIDI_OUT_2.SendNoteOff(msg.Channel, msg.Pitch, msg.Velocity);
        }

        public void PitchBend(PitchBendMessage msg) {
            MIDI_OUT_1.SendPitchBend(msg.Channel, msg.Value);
            MIDI_OUT_2.SendPitchBend(msg.Channel, msg.Value);
        }

        public void ProgramChange(ProgramChangeMessage msg) {
            MIDI_OUT_1.SendProgramChange(msg.Channel, msg.Instrument);
            MIDI_OUT_2.SendProgramChange(msg.Channel, msg.Instrument);
        }

        public void ControlChange(ControlChangeMessage msg) {
            MIDI_OUT_1.SendControlChange(msg.Channel, msg.Control, msg.Value);
            MIDI_OUT_2.SendControlChange(msg.Channel, msg.Control, msg.Value);
        }

        public void SysEx(SysExMessage msg) {
            MIDI_OUT_1.SendSysEx(msg.Data);
            MIDI_OUT_2.SendSysEx(msg.Data);
        }

    }
}
