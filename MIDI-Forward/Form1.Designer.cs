﻿namespace MIDI_Forward
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ddlMidiIn = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ddlMidiOut1 = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ddlMidiOut2 = new System.Windows.Forms.ComboBox();
            this.btnForward = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ddlMidiIn);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(297, 52);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "MIDI Input";
            // 
            // ddlMidiIn
            // 
            this.ddlMidiIn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlMidiIn.FormattingEnabled = true;
            this.ddlMidiIn.Location = new System.Drawing.Point(7, 20);
            this.ddlMidiIn.Name = "ddlMidiIn";
            this.ddlMidiIn.Size = new System.Drawing.Size(284, 21);
            this.ddlMidiIn.TabIndex = 0;
            this.ddlMidiIn.SelectedIndexChanged += new System.EventHandler(this.ddlMidiIn_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ddlMidiOut1);
            this.groupBox2.Location = new System.Drawing.Point(13, 71);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(297, 52);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "MIDI Out 1";
            // 
            // ddlMidiOut1
            // 
            this.ddlMidiOut1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlMidiOut1.FormattingEnabled = true;
            this.ddlMidiOut1.Location = new System.Drawing.Point(6, 19);
            this.ddlMidiOut1.Name = "ddlMidiOut1";
            this.ddlMidiOut1.Size = new System.Drawing.Size(284, 21);
            this.ddlMidiOut1.TabIndex = 1;
            this.ddlMidiOut1.SelectedIndexChanged += new System.EventHandler(this.ddlMidiOut1_SelectedIndexChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ddlMidiOut2);
            this.groupBox3.Location = new System.Drawing.Point(13, 129);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(297, 52);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "MIDI Out 2";
            // 
            // ddlMidiOut2
            // 
            this.ddlMidiOut2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlMidiOut2.FormattingEnabled = true;
            this.ddlMidiOut2.Location = new System.Drawing.Point(6, 19);
            this.ddlMidiOut2.Name = "ddlMidiOut2";
            this.ddlMidiOut2.Size = new System.Drawing.Size(284, 21);
            this.ddlMidiOut2.TabIndex = 2;
            this.ddlMidiOut2.SelectedIndexChanged += new System.EventHandler(this.ddlMidiOut2_SelectedIndexChanged);
            // 
            // btnForward
            // 
            this.btnForward.Location = new System.Drawing.Point(122, 199);
            this.btnForward.Name = "btnForward";
            this.btnForward.Size = new System.Drawing.Size(75, 23);
            this.btnForward.TabIndex = 3;
            this.btnForward.Text = "Foward";
            this.btnForward.UseVisualStyleBackColor = true;
            this.btnForward.Click += new System.EventHandler(this.btnForward_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(322, 234);
            this.Controls.Add(this.btnForward);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox ddlMidiIn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox ddlMidiOut1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox ddlMidiOut2;
        private System.Windows.Forms.Button btnForward;
    }
}

